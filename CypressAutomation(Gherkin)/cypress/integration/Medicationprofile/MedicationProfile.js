Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport
Cypress.on('uncaught:exception', (err, runnable) => {
  return false;
});
      // Scenario 1
      // Given('I Login', () => {
      //   cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
      //             cy.viewport(1920, 924);//setting your windows size
      //   })
      
      //   And('I visit admission page and Modal will Display', () => {
      //     cy.visit('https://qado.medisource.com/patient'); //visit page
      //     cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div').should('be.exist')
      //   });
      
      //   When('I click skip', () => { 
      //         cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div > div:nth-child(3) > div > a').click()
      //   });

      //   Then('Modal will disappear', () => {
      //     cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div').should('not.be.exist')
      //   });
      
      //   When('I input referral date, click auto assign and input SOC date', () => { 
      //     const dayjs = require('dayjs')
      //     cy.get('#refDate').type('05132022')
      //     cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > ng-form > fieldset > table > tbody > tr:nth-child(2) > td.oasis__answer > table > tbody > tr > td.global__table-question > div > label > input').click()
      //     cy.get('#pre_admission_date').type(dayjs().format('MM/DD/YYYY'));
      //   });
      
      //   And('I input in information', (datatable) => { 
      //     datatable.hashes().forEach(element => {
      //       cy.get('#last_name').type(element.firstname);
      //       cy.get('#first_name').type(element.lastname);
      //       cy.wait(5000)
      //     })
      //   });
      
      //   And('I input birthdate', () => { 
      //     cy.get('#birthdate').type('12082002')
      //   });
      
      
      //   And('I check male', () =>{
      //       cy.get('.custom-input-radio > :nth-child(1) > .ng-pristine').check();
      //   })
      
      //   And('I input in address', (datatable) => { 
      //   datatable.hashes().forEach(element => {
      //     cy.get('#main_line1').type(element.streetaddress);
      //     cy.get('#main_city').type(element.city);
      //   })
      // })

      //   And('Select state', () =>{
      //     cy.get('#main_state_chosen > .chosen-single').click(); //clicking dropdown state
      //     cy.get('#main_state_chosen > div > ul > li:nth-child(6)').click(); //clicking result
      //   })

      //   And('I input Zip Code', () =>{
      //     cy.get('#main_zipcode').type("91008");
      //   })

      //   And('Click Same as Patient Address Button', () =>{
      //     cy.get(':nth-child(25) > [colspan="2"] > .btn__dark-blue_icon-bordered').click();//clicking Same as Patient Address button
      //   })

      //   And('I select attending physician, primary assurance, type of admission and point of origin', () =>{
      //     //Attending Physician
      //     cy.get('#physician_attending_chosen > .chosen-single').click();
      //     cy.get('#physician_attending_chosen > div > ul > li:nth-child(2)').click();
      //     //Primary Insurance
      //     cy.get('#primary_insurance_chosen > .chosen-single').click();
      //     cy.get('#primary_insurance_chosen > div > ul > li:nth-child(2)').click();
      //     //Admission Type
      //     cy.get('#admissiontype_chosen > .chosen-single').click();
      //     cy.get('#admissiontype_chosen > div > ul > li:nth-child(1)').click();
      //     //Point of Origin
      //     cy.get('#point_of_origin_chosen > .chosen-single').click();
      //     cy.get('#point_of_origin_chosen > div > ul > li:nth-child(1)').click();
          
      //   })

      //   And('I input surgery and allergies', () =>{
      //     cy.get('#diagnosis_surgery').type('heart failure');
      //     cy.get('#diagnosis_allergies > .host > .tags > .ng-pristine').type('peanut{enter}');

      //     })

      //   And('I select registered nurse and case manager', () =>{
      //     //Registered Nurse
      //     cy.get('#cs_rn_chosen > .chosen-single').click();
      //     cy.get('#cs_rn_chosen > div > ul > li:nth-child(1)').click();//selected REGISTERED NURSE
      //     //Case Manager
      //     cy.get('#cs_cm_chosen > .chosen-single').click();
      //     cy.get('#cs_cm_chosen > div > ul > li:nth-child(1)').click();//selected CASE MANAGER
      //     })
  
      //   And('I click Save Button', () =>{
      //   //SAVE THE RECORD
      //   cy.get('.btn__success').click();
      //   cy.wait(10000)
      //   })

      //   Then('It go to Patient Dashboard', () =>{
      //     cy.url().should('contain', '/overview');
      //   })

      //   When('I click medication tab', () =>{
      //     cy.get('#profile-main-header > div > ul > li:nth-child(7) > a').click()
      //   })

      //   Then('It should generate and display medication profile', () =>{
      //     cy.url().should('contain', '/medication/profile/list');
      //     cy.get('#medicalprofile__table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(2)')
      //     .should('be.exist')
      //     .contains('Medication Profile (Start of Care')
      //   })

      // Scenario 2
      //   Given('I Login1', () => {
      //   cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
      //             cy.viewport(1920, 924);//setting your windows size
      //   })
      
      //   And('I visit admission page and Modal will Display', () => {
      //     cy.visit('https://qado.medisource.com/patient'); //visit page
      //     cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div').should('be.exist')
      //   });
      
      //   When('I click skip', () => { 
      //         cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div > div:nth-child(3) > div > a').click()
      //   });

      //   Then('Modal will disappear', () => {
      //     cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div').should('not.be.exist')
      //   });
      
      //   When('I input referral date, click auto assign and input SOC date', () => { 
      //     const dayjs = require('dayjs')
      //     cy.get('#refDate').type('05132022')
      //     cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > ng-form > fieldset > table > tbody > tr:nth-child(2) > td.oasis__answer > table > tbody > tr > td.global__table-question > div > label > input').click()
      //     cy.get('#pre_admission_date').type(dayjs().format('MM/DD/YYYY'));
      //   });
      
      //   And('I input in information', (datatable) => { 
      //     datatable.hashes().forEach(element => {
      //       cy.get('#last_name').type(element.firstname);
      //       cy.get('#first_name').type(element.lastname);
      //       cy.wait(5000)
      //     })
      //   });
      
      //   And('I input birthdate', () => { 
      //     cy.get('#birthdate').type('12082002')
      //   });
      
      
      //   And('I check male', () =>{
      //       cy.get('.custom-input-radio > :nth-child(1) > .ng-pristine').check();
      //   })
      
      //   And('I input in address', (datatable) => { 
      //   datatable.hashes().forEach(element => {
      //     cy.get('#main_line1').type(element.streetaddress);
      //     cy.get('#main_city').type(element.city);
      //   })
      // })

      //   And('Select state', () =>{
      //     cy.get('#main_state_chosen > .chosen-single').click(); //clicking dropdown state
      //     cy.get('#main_state_chosen > div > ul > li:nth-child(6)').click(); //clicking result
      //   })

      //   And('I input Zip Code', () =>{
      //     cy.get('#main_zipcode').type("91008");
      //   })

      //   And('Click Same as Patient Address Button', () =>{
      //     cy.get(':nth-child(25) > [colspan="2"] > .btn__dark-blue_icon-bordered').click();//clicking Same as Patient Address button
      //   })

      //   And('I select attending physician, primary assurance, type of admission and point of origin', () =>{
      //     //Attending Physician
      //     cy.get('#physician_attending_chosen > .chosen-single').click();
      //     cy.get('#physician_attending_chosen > div > ul > li:nth-child(2)').click();
      //     //Primary Insurance
      //     cy.get('#primary_insurance_chosen > .chosen-single').click();
      //     cy.get('#primary_insurance_chosen > div > ul > li:nth-child(2)').click();
      //     //Admission Type
      //     cy.get('#admissiontype_chosen > .chosen-single').click();
      //     cy.get('#admissiontype_chosen > div > ul > li:nth-child(1)').click();
      //     //Point of Origin
      //     cy.get('#point_of_origin_chosen > .chosen-single').click();
      //     cy.get('#point_of_origin_chosen > div > ul > li:nth-child(1)').click();
          
      //   })

      //   And('I input surgery and allergies', () =>{
      //     cy.get('#diagnosis_surgery').type('heart failure');
      //     cy.get('#diagnosis_allergies > .host > .tags > .ng-pristine').type('peanut{enter}');

      //     })

      //   And('I select registered nurse and case manager', () =>{
      //     //Registered Nurse
      //     cy.get('#cs_rn_chosen > .chosen-single').click();
      //     cy.get('#cs_rn_chosen > div > ul > li:nth-child(1)').click();//selected REGISTERED NURSE
      //     //Case Manager
      //     cy.get('#cs_cm_chosen > .chosen-single').click();
      //     cy.get('#cs_cm_chosen > div > ul > li:nth-child(1)').click();//selected CASE MANAGER
      //     })
  
      //   And('I click Save Button', () =>{
      //   //SAVE THE RECORD
      //   cy.get('.btn__success').click();
      //   cy.wait(10000)
      //   })

      //   Then('It go to Patient Dashboard', () =>{
      //     cy.url().should('contain', '/overview');
      //   })

      //   When('I click OASIS Start of Care', () =>{
      //     cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr.sampletd.pointer.globallist-item.tbl__row-default.ng-scope.lupa_first_half > td:nth-child(2) > a.ng-binding.ng-scope').click()
      //   })

      //   And('I click diagnosis tab', () =>{
      //     cy.get('#oasis-tabs > :nth-child(2)').click()
      //   })

      //   Then('It should prefill allergies', () =>{
      //     cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(3) > span')
      //     .should('be.exist')
      //     .contains('peanut')
      //   })

      // Scenario 3
        // Given('I Login2', () => {
        // cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        //           cy.viewport(1920, 924);//setting your windows size
        // })
      
        // And('I visit patient', () => {
        //   cy.visit('https://qado.medisource.com/patientcare/F22A067F-6217-478E-A24E-E6ADEC074718/C33A1641-9FE0-45B3-8ECA-B9926D6A5B86/overview'); //visit page
        // });

        // When('I click medication tab', () =>{
        //   cy.get('#profile-main-header > div > ul > li:nth-child(7) > a').click()
        // })

        // And('I click medication profile and reconcile medication profile', () =>{
        //   cy.get('#medicalprofile__table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(2)').click()
        //   cy.wait(5000)
        //   cy.get('.btn__success').click()
        // })

        // And('I click save without modifying data', () =>{
        //   cy.get('.btn__success.ng-scope').click()
        // })

        // Then('It should accepts and displays success message', () =>{
        //   cy.get('body > div.alert.alert-success.alert-dismissable.growl-animated')
        //   .should('be.exist')
        //   .contains('No changes have been made.')
        // })
              
        // Scenario 4
        // Given('I Login3', () => {
        //   cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        //                   cy.viewport(1920, 924);//setting your windows size
        //   })
              
        // And('I visit patient', () => {
        //   cy.visit('https://qado.medisource.com/patientcare/F22A067F-6217-478E-A24E-E6ADEC074718/C33A1641-9FE0-45B3-8ECA-B9926D6A5B86/overview'); //visit page
        //   cy.wait(10000)
        // });
        
        // When('I click medication tab', () =>{
        //   cy.get('#profile-main-header > div > ul > li:nth-child(7) > a').click()
        //   })
        
        // And('I click medication profile and reconcile medication profile', () =>{
        //   cy.get('#medicalprofile__table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(2)').click()
        //   cy.wait(5000)
        //   cy.get('.btn__success').click()
        //   cy.wait(10000)
        //   })

        // And('I click NKA on Allergies', () =>{
        //   cy.get('[style="padding: 5px 10px;"] > .checkbox > .a-height').click()
        //   })
        
        // And('I click save', () =>{
        //   cy.get('.btn__success.ng-scope').click()
        //   })
        
        // Then('It should accepts and displays success message', () =>{
        //   cy.get('body > div.alert.alert-success.alert-dismissable.growl-animated')
        //   .should('be.exist')
        //   .contains('Medication Profile has been successfully updated!')
        //   })

        // Scenario 5 NOT FINISHED
        // Given('I Login4', () => {
        //   cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        //                   cy.viewport(1920, 924);//setting your windows size
        //   })
              
        // And('I visit patient', () => {
        //   cy.visit('https://qado.medisource.com/patientcare/F22A067F-6217-478E-A24E-E6ADEC074718/C33A1641-9FE0-45B3-8ECA-B9926D6A5B86/overview'); //visit page
        //   });
        
        // When('I click medication tab', () =>{
        //   cy.get('#profile-main-header > div > ul > li:nth-child(7) > a').click()
        //   })
        
        // And('I click medication profile and reconcile medication profile', () =>{
        //   cy.get('#medicalprofile__table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(2)').click()
        //   cy.wait(5000)
        //   cy.get('.btn__success').click()
        //   cy.wait(10000)
        //   })    
        
        //   // Scenario 6 NOT FINISHED
        // Given('I Login5', () => {
        //   cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        //                   cy.viewport(1920, 924);//setting your windows size
        //   })
              
        // And('I visit patient', () => {
        //   cy.visit('https://qado.medisource.com/patientcare/F22A067F-6217-478E-A24E-E6ADEC074718/C33A1641-9FE0-45B3-8ECA-B9926D6A5B86/overview'); //visit page
        //   cy.wait(10000)
        // });
        
        // When('I click medication tab', () =>{
        //   cy.get('#profile-main-header > div > ul > li:nth-child(7) > a').click()
        //   })
        
        // And('I click medication profile and reconcile medication profile', () =>{
        //   cy.get('#medicalprofile__table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(2)').click()
        //   cy.wait(5000)
        //   cy.get('.btn__success').click()
        //   cy.wait(10000)
        //   })

        // And('I type on the medication field', () => {
        //   cy.get('#medadd0').type('A')
        //   });

        // Then('It should display list of suggested medications based on the entered data from the database if there is', () => {
        //   cy.get('.list-unstyled')
        //   .should('be.exist')
        //   });

        // Scenario 7 NOT FINISHED
        // Given('I Login6', () => {
        //   cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        //   cy.viewport(1920, 924);//setting your windows size
        //   })
              
        // And('I visit patient', () => {
        //   cy.visit('https://qado.medisource.com/patientcare/F22A067F-6217-478E-A24E-E6ADEC074718/C33A1641-9FE0-45B3-8ECA-B9926D6A5B86/overview'); //visit page
        //   cy.wait(10000)
        // });
        
        // When('I click medication tab', () =>{
        //   cy.get('#profile-main-header > div > ul > li:nth-child(7) > a').click()
        //   cy.wait(5000)
        // })
        
        // And('I click medication profile and reconcile medication profile', () =>{
        //   cy.get('#medicalprofile__table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(2)').click()
        //   cy.wait(5000)
        //   cy.get('.btn__success').click()
        //   cy.wait(10000)
        //   })

        // And('I type or select on the medication field is not on the suggestion', () => {
        //   cy.get('#medadd0').type('Test')
        //   });

        // Then('It should accept the entered medication', () => {
        //   cy.get('#medicationForm > div:nth-child(1) > div > div.col-sm-12.p-15.bg__wt > fieldset > div.ng-scope > div.hidden__column-container.scroll-medication.p-t-20.w-100 > table > tbody:nth-child(1) > tr.globallist-item.ng-scope.ng-valid-maxlength.ng-dirty.ng-valid-parse.ng-valid.ng-valid-required > td:nth-child(3) > div > div > div.ng-isolate-scope')
        //   .should('be.exist')
        //   });

        // Scenario 8 NOT FINISHED
        // Given('I Login7', () => {
        //   cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        //   cy.viewport(1920, 924);//setting your windows size
        //   })
              
        // And('I visit patient', () => {
        //   cy.visit('https://qado.medisource.com/patientcare/F22A067F-6217-478E-A24E-E6ADEC074718/C33A1641-9FE0-45B3-8ECA-B9926D6A5B86/overview'); //visit page
        //   cy.wait(10000)
        // });
        
        // When('I click medication tab', () =>{
        //   cy.get('#profile-main-header > div > ul > li:nth-child(7) > a').click()
        //   cy.wait(5000)
        // })
        
        // And('I click medication profile and reconcile medication profile', () =>{
        //   cy.get('#medicalprofile__table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(2)').click()
        //   cy.wait(5000)
        //   cy.get('.btn__success').click()
        //   cy.wait(10000)
        //   })

        // And('I type or select on the suggested medication while typing', () => {
        //   cy.get('#medadd0').type('abacavir (Ziagen){enter}')
        //   });

        // Then('Should accept and automatically display and prefill its Classification', () => {
        //   cy.get('#medicationForm > div:nth-child(1) > div > div.col-sm-12.p-15.bg__wt > fieldset > div.ng-scope > div.hidden__column-container.scroll-medication.p-t-20.w-100 > table > tbody:nth-child(1) > tr.globallist-item.ng-scope.ng-valid-maxlength.ng-dirty.ng-valid-parse.ng-valid.ng-valid-required > td:nth-child(3) > div > div > div.ng-isolate-scope')
        //   .should('be.exist')
        //   });

        // Scenario 9 NOT FINISHED
        // Given('I Login8', () => {
        //   cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        //   cy.viewport(1920, 924);//setting your windows size
        //   })
              
        // And('I visit patient', () => {
        //   cy.visit('https://qado.medisource.com/patientcare/F22A067F-6217-478E-A24E-E6ADEC074718/C33A1641-9FE0-45B3-8ECA-B9926D6A5B86/overview'); //visit page
        //   cy.wait(10000)
        // });
        
        // When('I click medication tab', () =>{
        //   cy.get('#profile-main-header > div > ul > li:nth-child(7) > a').click()
        //   cy.wait(5000)
        // })
        
        // And('I click medication profile and reconcile medication profile', () =>{
        //   cy.get('#medicalprofile__table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(2)').click()
        //   cy.wait(5000)
        //   cy.get('.btn__success').click()
        //   cy.wait(10000)
        //   })

        // And('I type startfill date and number of days', () => {
        //   cy.get('#addMed0').type('06022022')
        //   cy.get(':nth-child(8) > .global__txtbox').type('65')
        //   });

        // Then('Should automatically display its corresponding Date based on the entered # of Days', () => {
        //   cy.get('#addMedDcDate0')
        //   .should('be.exist')
        //   });

        // Scenario 10 NOT FINISHED
        // Given('I Login9', () => {
        //   cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        //   cy.viewport(1920, 924);//setting your windows size
        //   })
              
        // And('I visit patient', () => {
        //   cy.visit('https://qado.medisource.com/patientcare/F22A067F-6217-478E-A24E-E6ADEC074718/C33A1641-9FE0-45B3-8ECA-B9926D6A5B86/overview'); //visit page
        //   cy.wait(10000)
        // });
        
        // When('I click medication tab', () =>{
        //   cy.get('#profile-main-header > div > ul > li:nth-child(7) > a').click()
        //   cy.wait(5000)
        // })
        
        // And('I click medication profile and reconcile medication profile', () =>{
        //   cy.get('#medicalprofile__table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(2)').click()
        //   cy.wait(5000)
        //   cy.get('.btn__success').click()
        //   cy.wait(10000)
        //   })

        // And('I type startfill date and number of days', () => {
        //   cy.get('#addMed0').type('06022022')
        //   cy.get(':nth-child(8) > .global__txtbox').type('10')
        //   });

        // Then('Discontinue modal should popup/display', () => {
        //   });

        // // Scenario 11 NOT FINISHED
        // Given('I Login10', () => {
        //   cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        //   cy.viewport(1920, 924);//setting your windows size
        //   })
              
        // And('I visit patient', () => {
        //   cy.visit('https://qado.medisource.com/patientcare/F22A067F-6217-478E-A24E-E6ADEC074718/C33A1641-9FE0-45B3-8ECA-B9926D6A5B86/overview'); //visit page
        //   cy.wait(10000)
        // });
        
        // When('I click medication tab', () =>{
        //   cy.get('#profile-main-header > div > ul > li:nth-child(7) > a').click()
        //   cy.wait(5000)
        // })
        
        // And('I click medication profile and reconcile medication profile', () =>{
        //   cy.get('#medicalprofile__table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(2)').click()
        //   cy.wait(5000)
        //   cy.get('.btn__success').click()
        //   cy.wait(10000)
        //   })

        // And('I type startfill date and select DC date', () => {
        //   cy.get('#addMed0').type('06022022')
        //   cy.get('#addMedDcDate0').type('06202022')
        //   });

        // Then('It should automatically compute and display its # of Days', () => {
        //   cy.get(':nth-child(8) > .global__txtbox')
        //   .should('be.exist')
        // });

        // // Scenario 12 NOT FINISHED
        // Given('I Login11', () => {
        //   cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        //   cy.viewport(1920, 924);//setting your windows size
        //   })
              
        // And('I visit patient', () => {
        //   cy.visit('https://qado.medisource.com/patientcare/F22A067F-6217-478E-A24E-E6ADEC074718/C33A1641-9FE0-45B3-8ECA-B9926D6A5B86/overview'); //visit page
        //   cy.wait(10000)
        // });
        
        // When('I click medication tab', () =>{
        //   cy.get('#profile-main-header > div > ul > li:nth-child(7) > a').click()
        //   cy.wait(5000)
        // })
        
        // And('I click medication profile and reconcile medication profile', () =>{
        //   cy.get('#medicalprofile__table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(2)').click()
        //   cy.wait(5000)
        //   cy.get('.btn__success').click()
        //   cy.wait(10000)
        //   })

        // And('I type startfill date and select DC date', () => {
        //   cy.get('#addMed0').type('06022022')
        //   cy.get('#addMedDcDate0').type('06202022')
        //   });

        // Then('Discontinue modal should pop-up', () => {
       
        // });

        // Scenario 13 NOT FINISHED
        // Given('I Login12', () => {
        //   cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        //   cy.viewport(1920, 924);//setting your windows size
        //   })
              
        // And('I visit patient', () => {
        //   cy.visit('https://qado.medisource.com/patientcare/F22A067F-6217-478E-A24E-E6ADEC074718/C33A1641-9FE0-45B3-8ECA-B9926D6A5B86/overview'); //visit page
        //   cy.wait(10000)
        // });
        
        // When('I click medication tab', () =>{
        //   cy.get('#profile-main-header > div > ul > li:nth-child(7) > a').click()
        //   cy.wait(5000)
        // })
        
        // And('I click medication profile and reconcile medication profile', () =>{
        //   cy.get('#medicalprofile__table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(2)').click()
        //   cy.wait(5000)
        //   cy.get('.btn__success').click()
        //   cy.wait(10000)
        //   })

        // And('I type startfill date and select DC date', () => {
        //   cy.get('#addMed0').type('06022022')
        //   cy.get('#addMedDcDate0').type('06202022')
        //   });

        // Then('"Medication" word on the menus shuld become red to notify the clinician that they must check the Med Pro that has overlap the DC Date', () => {
        
        // });

        // // Scenario 14 NOT FINISHED
        // Given('I Login13', () => {
        //   cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        //   cy.viewport(1920, 924);//setting your windows size
        //   })
              
        // And('I visit patient', () => {
        //   cy.visit('https://qado.medisource.com/patientcare/F22A067F-6217-478E-A24E-E6ADEC074718/C33A1641-9FE0-45B3-8ECA-B9926D6A5B86/overview'); //visit page
        //   cy.wait(10000)
        // });
        
        // When('I click medication tab', () =>{
        //   cy.get('#profile-main-header > div > ul > li:nth-child(7) > a').click()
        //   cy.wait(5000)
        // })
        
        // And('I click medication profile and reconcile medication profile', () =>{
        //   cy.get('#medicalprofile__table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(2)').click()
        //   cy.wait(5000)
        //   cy.get('.btn__success').click()
        //   cy.wait(10000)
        //   })

        // And('I select RX on status', () => {
        //   cy.get('.medselect').click()
        //   cy.get('td.medselect_code > .radio').click()
        //   });

        // Then('These status SHOULD BE Required', () => {
        
        // });

        //  // Scenario 15 NOT FINISHED
        //  Given('I Login14', () => {
        //   cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        //   cy.viewport(1920, 924);//setting your windows size
        //   })
              
        // And('I visit patient', () => {
        //   cy.visit('https://qado.medisource.com/patientcare/F22A067F-6217-478E-A24E-E6ADEC074718/C33A1641-9FE0-45B3-8ECA-B9926D6A5B86/overview'); //visit page
        //   cy.wait(10000)
        // });
        
        // When('I click medication tab', () =>{
        //   cy.get('#profile-main-header > div > ul > li:nth-child(7) > a').click()
        //   cy.wait(5000)
        // })
        
        // And('I click medication profile and reconcile medication profile', () =>{
        //   cy.get('#medicalprofile__table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(2)').click()
        //   cy.wait(5000)
        //   cy.get('.btn__success').click()
        //   cy.wait(10000)
        //   })

        // And('I select RX on status', () => {
        //   cy.get('.medselect').click()
        //   cy.get('td.medselect_code > .radio').click()
        //   });

        // And('I click HLD', () => {
        //   cy.get('tbody > :nth-child(1) > :nth-child(3) > .checkbox').click()
        //   });

        //   And('I type Resume date', () => {
        //   cy.get('#addMedResume0').type('06022022')
        //   });

        // Then('"Medication" menu should have a highlight', () => {
        
        // });

         // Scenario 16 NOT FINISHED
         Given('I Login15', () => {
          cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
          cy.viewport(1920, 924);//setting your windows size
          })
              
        And('I visit patient', () => {
          cy.visit('https://qado.medisource.com/patientcare/F22A067F-6217-478E-A24E-E6ADEC074718/C33A1641-9FE0-45B3-8ECA-B9926D6A5B86/overview'); //visit page
          cy.wait(10000)
        });
        
        When('I click medication tab', () =>{
          cy.get('#profile-main-header > div > ul > li:nth-child(7) > a').click()
          cy.wait(5000)
        })
        
        And('I click medication profile and reconcile medication profile', () =>{
          cy.get('#medicalprofile__table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(2)').click()
          cy.wait(5000)
          cy.get('.btn__success').click()
          cy.wait(10000)
          })

        And('I select RX on status', () => {
          cy.get('.medselect').click()
          cy.get('td.medselect_code > .radio').click()
          });

        And('I click HLD', () => {
          cy.get('tbody > :nth-child(1) > :nth-child(3) > .checkbox').click()
          });

          And('I type Resume date', () => {
          cy.get('#addMedResume0').type('06022022')
          });

        And('I click save', () => {
          cy.get('.btn__success.ng-scope').click()
          });
  
        Then('Should automatically remve the Highlight on the Medication menu and on the View Medication Profile page', () => {
        
        });
        
         // Scenario 17 NOT FINISHED
         Given('I Login16', () => {
          cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
          cy.viewport(1920, 924);//setting your windows size
          })
              
        And('I visit patient', () => {
          cy.visit('https://qado.medisource.com/patientcare/F22A067F-6217-478E-A24E-E6ADEC074718/C33A1641-9FE0-45B3-8ECA-B9926D6A5B86/overview'); //visit page
          cy.wait(10000)
        });
        
        When('I click medication tab', () =>{
          cy.get('#profile-main-header > div > ul > li:nth-child(7) > a').click()
          cy.wait(5000)
        })
        
        And('I click medication profile and reconcile medication profile', () =>{
          cy.get('#medicalprofile__table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(2)').click()
          cy.wait(5000)
          cy.get('.btn__success').click()
          cy.wait(10000)
          })

        And('I select RX on status', () => {
          cy.get('.medselect').click()
          cy.get('td.medselect_code > .radio').click()
          });

        And('I click HLD', () => {
          cy.get('tbody > :nth-child(1) > :nth-child(3) > .checkbox').click()
          });

          And('I type Resume date', () => {
          cy.get('#addMedResume0').type('06022022')
          });

        And('I click save', () => {
          cy.get('.btn__success.ng-scope').click()
          });
  
        Then('Should automatically remve the Highlight on the Medication menu and on the View Medication Profile page', () => {
        
        });
        
                  
        
        
          

