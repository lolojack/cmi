Feature: Plotting of Task for RN-OASIS-D1 Transfer-not discharged
Scenario: Go to Patient List and select the added patient and plotting transfer
    Given I Login
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    
    # Schedule RN-OASIS-D1 Transfer (not discharged)
    And I schedule or click date for transfer not discharged
    And I input task date for transfer not discharged
    And I select task for transfer not discharged
    And I select clinician for transfer not discharged
    And I input visit date
    And I save task created for transfer not discharged
