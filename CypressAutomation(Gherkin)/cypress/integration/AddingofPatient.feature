Feature: Admission Test

Scenario: Adding Patient
    Given I Login
    And I visit admission page and Modal will Display
    When I click skip
    Then Modal will disappear
    When I input referral date and hours, click auto assign and input planned SOC date
    And I input in firstname, lastname, middleinitial, suffix and birthdate
    And I check male
    And I select marital status "Single", ethnicity "Asian" and language "English"
    And I input Social Security Number
    And I input streetaddress, additionaldirections, majorcrossstreet and city
    And I select state
    And I input zipcode, phone1, phone2 and email
    And I click same as patient address button
    And I input caregiver name and phone
    And I input name, relationship, emergencyphone1 and emergencyphone2 in emergency contact
    And I select attending physician, primary care physician and other physician
    And I select type and input policy or HIC number for primary insurance
    And I select type and input policy or HIC number for secondary insurance
    And I select type of admission, point of origin, type of referral and referral source for referral information
    And I select hospital or facility for hospitalization information
    And I input facility admit date and discharge date for hospitalization information
    And I input surgery and allergy for diagnoses and pre-admission orders
    And I select discipline of person completing assessment
    And I select registered nurse and case manager
    And I click save
    Then It will display a message that patient is successfully admitted