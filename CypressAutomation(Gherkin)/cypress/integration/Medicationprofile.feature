Feature: Medication Profile

#FINISHED
# Scenario: Check the Auto Generating Medication Profile Functionality
#     Given I Login
#     And I visit admission page and Modal will Display
#     When I click skip
#     Then Modal will disappear
   
#     #Patient Information section
#     When I input referral date, click auto assign and input SOC date
#     And I input in information
#         |  firstname  | lastname  |
#         |  Kovy | Sumera  |
#     And I input birthdate
#     And I check male
#     #Patient Address section
#     And I input in address
#         |  streetaddress  | city |
#         |  1752 Wilson Ave | Arcadia |
#     And Select state
#     And I input Zip Code
#     And Click Same as Patient Address Button
#     And I select attending physician, primary assurance, type of admission and point of origin
#     And I input surgery and allergies
#     And I select registered nurse and case manager
#     And I click Save Button
#     Then It go to Patient Dashboard
#     When I click medication tab
#     Then It should generate and display medication profile

#FINISHED
# Scenario: Check the prefilling of Allergies from Pre-admission
#     Given I Login1
#     And I visit admission page and Modal will Display
#     When I click skip
#     Then Modal will disappear
   
#     #Patient Information section
#     When I input referral date, click auto assign and input SOC date
#     And I input in information
#         |  firstname  | lastname  |
#         |  Kovy | Sumera  |
#     And I input birthdate
#     And I check male
#     #Patient Address section
#     And I input in address
#         |  streetaddress  | city |
#         |  1752 Wilson Ave | Arcadia |
#     And Select state
#     And I input Zip Code
#     And Click Same as Patient Address Button
#     And I select attending physician, primary assurance, type of admission and point of origin
#     And I input surgery and allergies
#     And I select registered nurse and case manager
#     And I click Save Button
#     Then It go to Patient Dashboard
#     When I click OASIS Start of Care
#     And I click diagnosis tab
#     Then It should prefill allergies

#FINISHED
# Scenario: Check the Saving functionality
#     Given I Login2
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And I click save without modifying data
#     Then It should accepts and displays success message

# FINISHED
# Scenario: Check the Saving functionality - Add Allergies
#     Given I Login3
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And I click NKA on Allergies
#     And I click save
#     Then It should accepts and displays success message

#NOT FINISHED
# Scenario: Check the Saving functionality - A modal should pop-up asking if the user wants to generate a Physician Order for the new added medication
#     Given I Login4
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And Fill up necessary field
#     And I click save 
#     Then A modal should pop-up asking if the user wants to generate a Physician Order for the new added medication

# FINISHED
# Scenario: Check the displaying of Medication Names/Brands upon typing - Not on the suggestion
#     Given I Login5
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And I type on the medication field
#     Then It should display list of suggested medications based on the entered data from the database if there is

#NOT FINISHED
# Scenario: Check the displaying of Medication Names/Brands upon typing - Select one on the suggested medication while typing
#     Given I Login6
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And I type or select on the medication field is not on the suggestion
#     Then  It should accept the entered medication

#NOT FINISHED
# Scenario: Check the displaying of Medication Names/Brands upon typing - Select one on the suggested medication while typing
#     Given I Login7
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And I type or select on the suggested medication while typing
#     Then Should accept and automatically display and prefill its Classification

#NOT FINISHED
# Scenario: Check the Discontinue functionality - Entered # of Days
#     Given I Login8
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And Fill up necessary field
#     And I type startfill date and number of days
#     Then Should automatically display its corresponding Date based on the entered # of Days


#NOT FINISHED
# Scenario: Check the Discontinue functionality - Entered #of Days is not beyond the current date
#     Given I Login9
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And Fill up necessary field
#     And I type startfill date and number of days
#     Then Discontinue modal should popup/display

#NOT FINISHED
# Scenario: Check the Discontinue functionality - Select DC Date
#     Given I Login10
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     # And Fill up necessary field
#     And I type startfill date and select DC date
#     Then It should automatically compute and display its # of Days

#NOT FINISHED
# Scenario: Check the Discontinue functionality - Discontinue modal should pop-up 
#     Given I Login11
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     # And Fill up necessary field
#     And I type startfill date and select DC date
#     Then Discontinue modal should pop-up

# NOT FINISHED
# Scenario: Check the Discontinue functionality - "Medication" word on the menus shuld become red to notify the clinician
#     Given I Login12
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     # And Fill up necessary field
#     And I type startfill date and select DC date
#     Then "Medication" word on the menus shuld become red to notify the clinician that they must check the Med Pro that has overlap the DC Date


#NOT FINISHED
# Scenario: Check the Status field functionality - Status SHOULD BE  Required 
#     Given I Login13
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And Fill up necessary field
#     And I select RX on status
#     Then These status SHOULD BE Required 

# #NOT FINISHED
# Scenario: Check the Status field functionality - "Medication" menu should have a highlight
#     Given I Login14
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And Fill up necessary field
#     And I select RX on status
#     And I click HLD
#     And I type Resume date
#     Then "Medication" menu should have a highlight

#NOT FINISHED
# Scenario: Check the Status field functionality - Should automatically remve the Highlight on the Medication menu and on the View Medication Profile page
#     Given I Login15
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And Fill up necessary field
#     And I select RX on status
#     And I click HLD
#     And I type Resume date
#     And I click save
#     Then Should automatically remve the Highlight on the Medication menu and on the View Medication Profile page

#NOT FINISHED
Scenario: Check the Status field functionality - UFO - Until Further Order
    Given I Login16
    And I visit patient
    When I click medication tab
    And I click medication profile and reconcile medication profile
    # And Fill up necessary field
    And I select RX
    And I click HLD
    And I click UFO - Until Further Order
    And I click save
    Then "Medication" menu should have a highlight

# NOT FINISHED
# Scenario: Check the Status field functionality - Should automatically uncheck the NEW on the Status field after 30days
#     Given I Login14
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And Add new medication
#     And I select RX
#     And I click NEW on the Status field
#     And I click save
#     Then 

# NOT FINISHED
# Scenario: Check the Status field functionality - Should automatically display the "Reconcile Discrepancies" button beside the "Treatment Order Processor"
#     Given I Login15
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And Fill up necessary field
#     And I click show all columns
#     And I select Discrepancies
#     And I click save
#     Then It should automatically display the "Reconcile Discrepancies" button beside the "Treatment Order Processor"

#NOT FINISHED
# Scenario: Check the Status field functionality - TAKE ONLY AS NEEDED 
#     Given I Login16
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And Fill up necessary field
#     And I click show all columns
#     And I select Discrepancies
#     And I input "As Needed" on the Frequency field
#     And I click save
#     And I click print icon and click Medication Schedule for Patient
#     Then 

#NOT FINISHED
# Scenario: Check the Status field functionality - TIME-SPECIFIC
#     Given I Login16
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And Fill up necessary field
#     And I click show all columns
#     And I select Discrepancies
#     And I input specific time "every 3 hours" on the Frequency field
#     And I click save
#     And I click print icon and click Medication Schedule for Patient
#     Then 

#NOT FINISHED
# Scenario: Check the Print functionality
#     Given I Login17 
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And I fill up necessary field
#     And I click save
#     And I click print icon
#     And click Patient Copy
#     Then It should should be able to load Patient Copy
#     When I click Medication Schedule for Patient
#     Then It should should be able to load Medication Schedule for Patient
#     When I click Agency Copy
#     Then It should should be able to load Agency Copy

#NOT FINISHED
# Scenario:  Check the Route field functionality
#     Given I Login18
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And I fill up necessary field
#     And I click show all columns
#     And I select "intravenous" , "subcutaneous" and "intramuscular" on Route
#     And click save
#     Then It will display on the PROCEDURES menu on the Patient Dashboard

# NOT FINISHED
# Scenario:  Check delete functionality
#     Given I Login19
#     And I visit patient
#     When I click medication tab
#     And I click checkbox beside the medication
#     And click Delete floating button
#     Then It should only be able to delete added medication within 24hour after adding the mediation

#NOT FINISHED
# Scenario:  Check Treatment Order Editor functionality
#     Given I Login20
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And I click Treatment Order Editor button - make sure you have already pinned wound
#     And I fill up needed treatment
#     And I click insert and save
#     Then It should apply on the Preview Report and Print report on the selected wound# on the Wound Management\
#     And It should also display at medication on the PROCEDURE menu

#NOT FINISHED
# Scenario:  Check Discontinue functionality - Should be able to UNDO the discontinued medication within 24 hours only
#     Given I Login21
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And I click Discontinue tab
#     And I click checkbox beside the discontinued medication
#     And I click undo
#     And Check medication tab
#     Then It should be able to UNDO the discontinued medication within 24 hours only
    
# Scenario:  Check Discontinue functionality - Should automatically go back to the Medication Tab
#     Given I Login22
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And I click Discontinue tab
#     And I click checkbox beside the discontinued medication
#     And I click undo
#     Then It should automatically go back to the Medication Tab

# #NOT FINISHED
# Scenario:  Verify Allergy Profile Process
#     Given I Login23
#     And I visit patient
#     When I click medication tab
#     And I click medication profile and reconcile medication profile
#     And I click Discontinue tab
#     And I click checkbox beside the discontinued medication
#     And I click undo

