Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
  });

  const dayjs = require('dayjs')
  
  
  Given('I Login', () => {
    cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });
   And('I click in-patient tab', () => {
       cy.get('#content > data > div > div.card > div > div.patientcare__nav > div > ul > li:nth-child(2) > a').click() //click in-patient tab
   });

    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jow P. Jr')  //search the added patient 
        cy.wait(5000) 
    });

    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(5000)
    });

    And('I click Transfer - not discharged', () => {
        cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(19) > td:nth-child(2) > a.ng-binding.ng-scope').click();//click Transfer (not discharged)
        cy.wait(5000)
    });

    And('I click Edit Button', () => {
        cy.get('.btn__warning').click();//click Edit Button
        cy.wait(5000)
    });

    //DEMOGRAPHICS/CLINICAL RECORD TAB Start -------------
    And('I input time in and time out', () => {
        cy.get('#ti').type('1200') //inputting time in
        cy.get('#to').type('1900') //inputting time out
    });
    And('I input date assessment completed', () => {
        cy.get('#M0090_INFO_COMPLETED_DT').type(dayjs().add(45, 'day').format('MM/DD/YYYY')) //inputting date assessment completed
    });
    And('I selecting 1 Medicare traditional fee for service for Current Payment Sources for Home Care', () => {
        cy.get('#M0150_CPAY_MCARE_FFS > input').click()
    });
    And('I selecting 1 for Influenza Vaccine Data Collection Period', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(26) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click() //clicking 1
    });
    And('I selecting 1 for Influenza Vaccine Received', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(28) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(1) > label > input').click() //clicking 1
    });
    And('I click No for Pneumococcal Vaccine', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(30) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(1) > label > input').click() //clicking NO
    });
    And('I click 3 for Reason Pneumococcal Vaccine not received', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(32) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(3) > label > input').click() //clicking 3
    });
    And('I click save button', () => {
        //SAVE BUTTON
        cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
        cy.wait(5000)    
    });

    // DEMOGRAPHICS/CLINICAL RECORD TAB End -------------

Given('I Login1', () => {
    cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });
    And('I click in-patient tab', () => {
        cy.get('#content > data > div > div.card > div > div.patientcare__nav > div > ul > li:nth-child(2) > a').click() //click in-patient tab
    });
    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jow P. Jr')  //search the added patient 
        cy.wait(5000) 
    });

    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(5000)
    });

    And('I click Transfer - not discharged', () => {
        cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(19) > td:nth-child(2) > a.ng-binding.ng-scope').click();//click Transfer (not discharged)
        cy.wait(5000)
    });

    And('I click Edit Button', () => {
        cy.get('.btn__warning').click();//click Edit Button
        cy.wait(5000)
    });
    And('I click Medication Tab', () => {
        cy.get('#oasis-tabs > label:nth-child(2)').click() //clicking medication tab
    });

    //MEDICATION TAB Start -------------
    And('I click 1 - yes for M2005', () => {
        //(M2005) - Medication Intervention: Did the agency contact and complete physician (or physician-designee) prescribed/recommended actions by midnight of the next calendar day each time potential clinically significant medication issues were identified since the SOC/ROC?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(1) > tbody > tr:nth-child(2) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click() //clicking 1-Yes
    });
    And('I click 1 - yes for M2016', () => {
        //(M2016) - Patient/Caregiver Drug Education Intervention: At the time of, or at any time since the most recent SOC/ROC assessment, was the patient/caregiver instructed by agency staff or other health care provider to monitor the effectiveness of drug therapy, adverse drug reactions, and significant side effects, and how and when to report problems that may occur?
        cy.get('#M2016_DRUG_EDCTN_INTRVTN > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click() //clicking 1-Yes
    });
    And('I click save button', () => {
        //SAVE BUTTON
        cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
        cy.wait(5000)    
    });

    //MEDICATION TAB End -------------

    Given('I Login2', () => {
    cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });
    And('I click in-patient tab', () => {
        cy.get('#content > data > div > div.card > div > div.patientcare__nav > div > ul > li:nth-child(2) > a').click() //click in-patient tab
    });
    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jow P. Jr')  //search the added patient 
        cy.wait(5000) 
    });

    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(10000)
    });

    And('I click Transfer - not discharged', () => {
        cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(19) > td:nth-child(2) > a.ng-binding.ng-scope').click();//click Transfer (not discharged)
        cy.wait(5000)
    });

    And('I click Edit Button', () => {
        cy.get('.btn__warning').click();//click Edit Button
        cy.wait(5000)
    });
    And('I click Emergent Care Tab', () => {
        cy.get('#oasis-tabs > label:nth-child(3)').click() //clicking medication tab
    });

    //EMERGENT CARE TAB Start -------------
    And('I click Yes, used hospital emergency department WITHOUT hospital admission for M2301', () => {
        cy.get('#M2301_EMER_USE_AFTR_LAST_ASMT > td.oasis__answer.v-top > table > tbody > tr > td > div > div:nth-child(2) > label > input').click()
    });
    And('I select all that apply for M2310', () => {
        cy.get('#M2310_ECR_MEDICATION').click()
        cy.get('#M2310_ECR_HYPOGLYC').click()
        cy.get('#M2310_ECR_OTHER').click()
    });
    //DATA ITEMS COLLECTED AT INPATIENT FACILITY ADMISSION OR AGENCY DISCHARGE ONLY
    //(M2401) - Intervention Synopsis (clicking yes to all)
    And('I click yes to all Intervention Synopsis', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(3) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(4) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(5) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(6) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(7) > td:nth-child(3) > div > label > input').click()
    });
    And('I click 1 - Hospital for M2410', () => {
        // /(M2410) - To which Inpatient Facility has the patient been admitted?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(9) > td.oasis__answer.v-top > table > tbody > tr > td > div > div:nth-child(1) > label > input').click() //clicking 1 - Hospital
    });
    And('I input Discharge-Transfer-Death Date', () => {
        //(M0906) - Discharge/Transfer/Death Date
        cy.get('#M0906_DC_TRAN_DTH_DT_DATEPICKER').type('05292022')
    });
    //Section J: Health Conditions
    //J1800 - Any Falls Since SOC/ROC
    And('I click yes for J1800', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(14) > td.oasis__answer.v-top > table > tbody > tr > td > div > div:nth-child(2) > label > input').click() //clicking 1-YES
    });
    //J1900 - Number of Falls Since SOC/ROC
    And('I input 1 for A - No Injury', () => {
        cy.get('[data-ng-model="data.J1900A"]').type('1')
    });
    And('I input 1 for B - Injury', () => {
        cy.get('#J1900B').type('1') //inputting 1 B. Injury (except major)
    });
    And('I input 1 for C - Major Injury', () => {
        cy.get('#J1900C').type('1') //inputting 1 C. Major injury
    });
    And('I click save button', () => {
        //SAVE BUTTON
        cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
        cy.wait(5000)    
    });

    //EMERGENT CARE TAB End -------------













    



