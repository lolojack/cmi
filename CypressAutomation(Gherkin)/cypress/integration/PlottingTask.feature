Feature: Plotting Task

Scenario: Go to Patient List and select the added patient and plotting all task
    Given I Login
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    
    # Schedule LVN/LPN - Skilled Visit
    And I schedule or click date for LVN or LPN Skilled Visit
    And I input task date for LVN or LPN Skilled Visit
    And I select task for LVN or LPN Skilled Visit
    And I select clinician for LVN or LPN Skilled Visit
    And I uncheck Create an MD Order for LVN or LPN Skilled Visit
    And I save task created for LVN or LPN Skilled Visit
    
    # Schedule LVN/LPN - Wound Visit
    And I schedule or click date for LVN or LPN Wound Visit
    And I input task date for LVN or LPN Wound Visit
    And I select task for LVN or LPN Wound Visit
    And I select clinician for LVN or LPN Wound Visit
    And I uncheck Create an MD Order for LVN or LPN Wound Visit
    And I save task created for LVN or LPN Wound Visit

    # Schedule PRN - Skilled Visit
    And I schedule or click date for PRN Skilled Visit
    And I input task date for PRN Skilled Visit
    And I select task for PRN Skilled Visit
    And I select clinician for PRN Skilled Visit
    And I uncheck Create an MD Order for PRN Skilled Visit
    And I save task created for PRN Skilled Visit

    # Schedule RN - Education Visit
    And I schedule or click date for RN Education Visit
    And I input task date for RN Education Visit
    And I select task for RN Education Visit
    And I select clinician for RN Education Visit
    And I uncheck Create an MD Order for RN Education Visit
    And I save task created for RN Education Visit

    # Schedule RN - IV Visit
    And I schedule or click date for RN IV Visit
    And I input task date for RN IV Visit
    And I select task for RN IV Visit
    And I select clinician for RN IV Visit
    And I uncheck Create an MD Order for RN IV Visit
    And I save task created for RN IV Visit

    # Schedule RN - Joint Supervisory (CHHA)
    And I schedule or click date for RN Joint Supervisory CHHA
    And I input task date for RN Joint Supervisory CHHA
    And I select task for RN Joint Supervisory CHHA
    And I select clinician for RN Joint Supervisory CHHA
    And I uncheck Create an MD Order for RN Joint Supervisory CHHA
    And I save task created for RN Joint Supervisory CHHA

    # Schedule RN - Joint Supervisory (LVN)
    And I schedule or click date for RN Joint Supervisory LVN
    And I input task date for RN Joint Supervisory LVN
    And I select task for RN Joint Supervisory LVN
    And I select clinician for RN Joint Supervisory LVN
    And I uncheck Create an MD Order for RN Joint Supervisory LVN
    And I save task created for RN Joint Supervisory LVN

    # Schedule RN - Skilled Visit
    And I schedule or click date for RN Skilled Visit
    And I input task date for RN Skilled Visit
    And I select task for RN Skilled Visit
    And I select clinician for RN Skilled Visit
    And I uncheck Create an MD Order for RN Skilled Visit
    And I save task created for RN Skilled Visit

    # Schedule RN - Supervisory Visit
    And I schedule or click date for RN Supervisory Visit
    And I input task date for RN Supervisory Visit
    And I select task for RN Supervisory Visit
    And I select clinician for RN Supervisory Visit
    And I uncheck Create an MD Order for RN Supervisory Visit
    And I save task created for RN Supervisory Visit

    # Schedule RN - Wound Visit
    And I schedule or click date for RN Wound Visit
    And I input task date for RN Wound Visit
    And I select task for RN Wound Visit
    And I select clinician for RN Wound Visit
    And I uncheck Create an MD Order for RN Wound Visit
    And I save task created for RN Wound Visit

    # Schedule PT - Initial Eval
    And I schedule or click date for PT Initial Eval
    And I input task date for PT Initial Eval
    And I select task for PT Initial Eval
    And I select clinician for PT Initial Eval
    And I uncheck Create an MD Order for PT Initial Eval
    And I save task created for PT Initial Eval

    # Schedule OT - Initial Eval
    And I schedule or click date for OT Initial Eval
    And I input task date for OT Initial Eval
    And I select task for OT Initial Eval
    And I select clinician for OT Initial Eval
    And I uncheck Create an MD Order for OT Initial Eval
    And I save task created for OT Initial Eval

    # Schedule ST - Initial Eval
    And I schedule or click date for ST Initial Eval
    And I input task date for ST Initial Eval
    And I select task for ST Initial Eval
    And I select clinician for ST Initial Eval
    And I uncheck Create an MD Order for ST Initial Eval
    And I save task created for ST Initial Eval

    # Schedule MSW - Assessment
    And I schedule or click date for MSW Assessment
    And I input task date for MSW Assessment
    And I select task for MSW Assessment
    And I select clinician for MSW Assessment
    And I uncheck Create an MD Order for MSW Assessment
    And I save task created for MSW Assessment

    # Schedule MSW - Follow-up Visit
    And I schedule or click date for MSW Follow up Visit
    And I input task date for MSW Follow up Visit
    And I select task for MSW Follow up Visit
    And I select clinician for MSW Follow up Visit
    And I uncheck Create an MD Order for MSW Follow up Visit
    And I save task created for MSW Follow up Visit

    # Schedule CHHA - HHA Visit
    And I schedule or click date for CHHA HHA Visit
    And I input task date for CHHA HHA Visit
    And I select task for CHHA HHA Visit
    And I select clinician for CHHA HHA Visit
    And I uncheck Create an MD Order for CHHA HHA Visit
    And I save task created for CHHA HHA Visit











