Feature: Automating Entry Form of Care Summary

Scenario: Automating Entry Form of 30-Day Care Summary
    Given I Login
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click Communication Note
    And I click new

    # 30-Day Care Summary Start -------------
    And I click 30-Day Care Summary
    And I input conference date
    And I input other diagnoses
    #SCIC/Change in Diagnosis/Events/Physician Orders
    And I select all SCIC-Change in Diagnosis-Events-Physician Orders
    And I input other and comment
    #VS Monitor
    And I input comment for VS Monitor
    #Medication Management
    And I click yes for Patient compliant and Patient adherent
    #Laboratory/Diagnostic Tests
    And I input comment for Laboratory or Diagnostic Tests
    #Procedures
    And I input comment for Procedures
    #Assessment Summary
    And I input comment for sensory status, integumentary, endocrine, respiratory, cardiovascular, Upper GI, Lower GI, Genitourinary, Neurologic and Musculoskeletal
    And I input comment for Assessment Summary
    And I input reasons for home health
    And I input homebound status
    And I input caregiving status
    #Physician Orders for Disciplines and Ancillary Referrals
    And I input comment for Physician Orders for Disciplines and Ancillary Referrals
    And I input care summary
    And I input progress towards goals
    And I input recommendations
    And I select team conference reviewers
    And I click save button
    # 30-Day Care Summary End -------------

Scenario: Automating Entry Form of Transfer Summary
    Given I Login1
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click Communication Note
    And I click new

    # Transfer Summary Start -------------
    And I click Transfer Summary
    And I input transfer date
    And I select physician
    #Transfer to (click all)
    And I select all for transfer
    #Reason for Transfer (click all)
    And I select all for reasons for transfer
    #Current Physical, Mental and Emotional Status (click all)
    And I select all for Current Physical, Mental and Emotional Status
    #Current Functional Status
    And I click independent for ADL
    And I click independent for mobility
    #Assistive Device: (click all)
    And I select all for assistive device
    And I input summary of care provided
    #Disciplines/visits provided
    And I select all for disciplines or visits provided
    #Transfer Notification Date
    And I input date for physician
    And I input date for Patient or Family or Caregiver
    #Send to MD via
    And I click fax for send to MD via
    And I click save button
    # Transfer Summary End -------------

Scenario: Automating Entry Form of Discharge Summary
    Given I Login2
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click Communication Note
    And I click new

    # Discharge Summary Start -------------
    And I click Discharge Summary
    And I input discharge date
    And I input secondary diagnoses
    And I select all for home health admission
    And I select all for reasons for discharge
    And I select all for Current Physical, Mental and Emotional Status
    # Current Functional Status
    And I click independent for ADL
    And I click independent for mobility
    #Assistive Device: (click all)
    And I select all for assistive device
    And I click 1 for discharge disposition
    And I input summary of care provided
    #Disciplines/visits provided
    And I select all for disciplines or visits provided
    And I click goals met for Treatment Goals Achieved
    #Discharge Notification Date
    And I input date for physician
    And I input date for Patient or Family or Caregiver
    #Send to MD via
    And I click fax for send to MD via
    And I click save button
    # Discharge Summary End -------------

Scenario: Automating Entry Form of Discharge Instruction
    Given I Login3
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click Communication Note
    And I click new

    # Discharge Instruction Start -------------
    And I click Discharge Instruction
    And I input discharge date
    And I select physician
    # We are pleased to have provided service to you. The following discharge instructions were reviewed with you and/or your caregiver/s during the final visit by agency staff. You are to:
    # Keep your scheduled appointment with Dr.
    And I select doctor 
    And I input date appointment
    And I select doctor1
    And I input date appointment1
    And I click Continue to take the medications as prescribed by your physician
    And I click Medication list attached
    And I input Additional medication instructions
    And I input additional instruction
    And I input wound care instruction
    And I input Follow through with community resource or organization to which you been referred
    And I input other instruction
    And I click save button
    # Discharge Instruction End -------------

Scenario: Automating Entry Form of Case Conference
    Given I Login4
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click Communication Note
    And I click new

    # Case Conference Start -------------
    And I click Case Conference
    And I input date created
    And I input home health reason
    And I input purpose of the conference
    And I click and input for Assessment of patient status and current needs by
    And I click Plan of actions to be taken, by whom and timeframe
    #Plan of actions to be taken, by whom and timeframe
    And I input all Plan of actions to be taken
    And I input all By whom
    And I input all Time frame, if any
    #Case Conference Participants (click all)
    And I select all Case Conference Participants
    And I input comment
    And I click save button
    # Case Conference End -------------







