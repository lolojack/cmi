Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
  });

  const dayjs = require('dayjs')
  
  
  Given('I Login', () => {
    cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jow P. Jr')  //search the added patient 
        cy.wait(5000) 
    });

    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(5000)
    });

    // Schedule LVN/LPN - Skilled Visit
    And('I schedule or click date for LVN or LPN Skilled Visit', () => {
        cy.get('#save > li > button').click()//clicking the calendar overly icon
    });
    And('I input task date for LVN or LPN Skilled Visit', () => {
        cy.get('#selecteddate').type(dayjs().add(2, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
    });
    And('I select task for LVN or LPN Skilled Visit', () => {
        cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
        cy.wait(5000)
        cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(6)').click()//select task
    });
    And('I select clinician for LVN or LPN Skilled Visit', () => {
        cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
        cy.wait(5000)
        cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
    });
    And('I uncheck Create an MD Order for LVN or LPN Skilled Visit', () => {
        cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
    });
    And('I save task created for LVN or LPN Skilled Visit', () => {
        cy.get('.mhc__btn-affirmative').click()//save task created
        cy.wait(10000)
    });

    // Schedule LVN/LPN - Wound Visit
    And('I schedule or click date for LVN or LPN Wound Visit', () => {
        cy.get('#save > li > button').click()//clicking the calendar overly icon
    });
    And('I input task date for LVN or LPN Wound Visit', () => {
        cy.get('#selecteddate').type(dayjs().add(3, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
    });
    And('I select task for LVN or LPN Wound Visit', () => {
        cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
        cy.wait(5000)
        cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(7)').click()//select task
    });
    And('I select clinician for LVN or LPN Wound Visit', () => {
        cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
        cy.wait(5000)
        cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
    });
    And('I uncheck Create an MD Order for LVN or LPN Wound Visit', () => {
        cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
    });
    And('I save task created for LVN or LPN Wound Visit', () => {
        cy.get('.mhc__btn-affirmative').click()//save task created
        cy.wait(10000)
    });
    
    // Schedule PRN - Skilled Visit
    And('I schedule or click date for PRN Skilled Visit', () => {
        cy.get('#save > li > button').click()//clicking the calendar overly icon
    });
    And('I input task date for PRN Skilled Visit', () => {
        cy.get('#selecteddate').type(dayjs().add(4, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
    });
    And('I select task for PRN Skilled Visit', () => {
        cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
        cy.wait(5000)
        cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(8)').click()//select task
    });
    And('I select clinician for PRN Skilled Visit', () => {
        cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
        cy.wait(5000)
        cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
    });
    And('I uncheck Create an MD Order for PRN Skilled Visit', () => {
        cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
    });
    And('I save task created for PRN Skilled Visit', () => {
        cy.get('.mhc__btn-affirmative').click()//save task created
        cy.wait(10000)
    });

    // Schedule RN - Education Visit
    And('I schedule or click date for RN Education Visit', () => {
        cy.get('#save > li > button').click()//clicking the calendar overly icon
    });
    And('I input task date for RN Education Visit', () => {
        cy.get('#selecteddate').type(dayjs().add(5, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
    });
    And('I select task for RN Education Visit', () => {
        cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
        cy.wait(5000)
        cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(9)').click()//select task
    });
    And('I select clinician for RN Education Visit', () => {
        cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
        cy.wait(5000)
        cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
    });
    And('I uncheck Create an MD Order for RN Education Visit', () => {
        cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
    });
    And('I save task created for RN Education Visit', () => {
        cy.get('.mhc__btn-affirmative').click()//save task created
        cy.wait(10000)
    });

    // Schedule RN - IV Visit
    And('I schedule or click date for RN IV Visit', () => {
        cy.get('#save > li > button').click()//clicking the calendar overly icon
    });
    And('I input task date for RN IV Visit', () => {
        cy.get('#selecteddate').type(dayjs().add(8, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
    });
    And('I select task for RN IV Visit', () => {
        cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
        cy.wait(5000)
        cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(10)').click()//select task
    });
    And('I select clinician for RN IV Visit', () => {
        cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
        cy.wait(5000)
        cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
    });
    And('I uncheck Create an MD Order for RN IV Visit', () => {
        cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
    });
    And('I save task created for RN IV Visit', () => {
        cy.get('.mhc__btn-affirmative').click()//save task created
        cy.wait(10000)
    });

    // Schedule RN - Joint Supervisory (CHHA)
    And('I schedule or click date for RN Joint Supervisory CHHA', () => {
        cy.get('#save > li > button').click()//clicking the calendar overly icon
    });
    And('I input task date for RN Joint Supervisory CHHA', () => {
        cy.get('#selecteddate').type(dayjs().add(9, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
    });
    And('I select task for RN Joint Supervisory CHHA', () => {
        cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
        cy.wait(5000)
        cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(11)').click()//select task
    });
    And('I select clinician for RN Joint Supervisory CHHA', () => {
        cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
        cy.wait(5000)
        cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
    });
    And('I uncheck Create an MD Order for RN Joint Supervisory CHHA', () => {
        cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
    });
    And('I save task created for RN Joint Supervisory CHHA', () => {
        cy.get('.mhc__btn-affirmative').click()//save task created
        cy.wait(10000)
    });

    // Schedule RN - Joint Supervisory (LVN)
    And('I schedule or click date for RN Joint Supervisory LVN', () => {
        cy.get('#save > li > button').click()//clicking the calendar overly icon
    });
    And('I input task date for RN Joint Supervisory LVN', () => {
        cy.get('#selecteddate').type(dayjs().add(10, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
    });
    And('I select task for RN Joint Supervisory LVN', () => {
        cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
        cy.wait(5000)
        cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(12)').click()//select task
    });
    And('I select clinician for RN Joint Supervisory LVN', () => {
        cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
        cy.wait(5000)
        cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
    });
    And('I uncheck Create an MD Order for RN Joint Supervisory LVN', () => {
        cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
    });
    And('I save task created for RN Joint Supervisory (LVN)', () => {
        cy.get('.mhc__btn-affirmative').click()//save task created
        cy.wait(10000)
    });

    // Schedule RN - Skilled Visit
    And('I schedule or click date for RN Skilled Visit', () => {
        cy.get('#save > li > button').click()//clicking the calendar overly icon
    });
    And('I input task date for RN Skilled Visit', () => {
        cy.get('#selecteddate').type(dayjs().add(11, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
    });
    And('I select task for RN Skilled Visit', () => {
        cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
        cy.wait(5000)
        cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(13)').click()//select task
    });
    And('I select clinician for RN Skilled Visit', () => {
        cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
        cy.wait(5000)
        cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
    });
    And('I uncheck Create an MD Order for RN Skilled Visit', () => {
        cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
    });
    And('I save task created for RN Skilled Visit', () => {
        cy.get('.mhc__btn-affirmative').click()//save task created
        cy.wait(10000)
    });

    // Schedule RN - Supervisory Visit
    And('I schedule or click date for RN Supervisory Visit', () => {
        cy.get('#save > li > button').click()//clicking the calendar overly icon
    });
    And('I input task date for RN Supervisory Visit', () => {
        cy.get('#selecteddate').type(dayjs().add(12, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
    });
    And('I select task for RN Supervisory Visit', () => {
        cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
        cy.wait(5000)
        cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(14)').click()//select task
    });
    And('I select clinician for RN Supervisory Visit', () => {
        cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
        cy.wait(5000)
        cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
    });
    And('I uncheck Create an MD Order for RN Supervisory Visit', () => {
        cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
    });
    And('I save task created for RN Supervisory Visit', () => {
        cy.get('.mhc__btn-affirmative').click()//save task created
        cy.wait(10000)
    });

    // Schedule RN - Wound Visit
    And('I schedule or click date for RN Wound Visit', () => {
        cy.get('#save > li > button').click()//clicking the calendar overly icon
    });
    And('I input task date for RN Wound Visit', () => {
        cy.get('#selecteddate').type(dayjs().add(14, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
    });
    And('I select task for RN Wound Visit', () => {
        cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
        cy.wait(5000)
        cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(15)').click()//select task
    });
    And('I select clinician for RN Wound Visit', () => {
        cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
        cy.wait(5000)
        cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
    });
    And('I uncheck Create an MD Order for RN Wound Visit', () => {
        cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
    });
    And('I save task created for RN Wound Visit', () => {
        cy.get('.mhc__btn-affirmative').click()//save task created
        cy.wait(10000)
    });

    // Schedule PT - Initial Eval
    And('I schedule or click date for PT Initial Eval', () => {
        cy.get('#save > li > button').click()//clicking the calendar overly icon
    });
    And('I input task date for PT Initial Eval', () => {
        cy.get('#selecteddate').type(dayjs().add(19, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
    });
    And('I select task for PT Initial Eval', () => {
        cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
        cy.wait(5000)
        cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(17)').click()//select task
    });
    And('I select clinician for PT Initial Eval', () => {
        cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
        cy.wait(5000)
        cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
    });
    And('I uncheck Create an MD Order for PT Initial Eval', () => {
        cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
    });
    And('I save task created for PT Initial Eval', () => {
        cy.get('.mhc__btn-affirmative').click()//save task created
        cy.wait(10000)
    });

    // Schedule OT - Initial Eval
    And('I schedule or click date for OT Initial Eval', () => {
        cy.get('#save > li > button').click()//clicking the calendar overly icon
    });
    And('I input task date for OT Initial Eval', () => {
        cy.get('#selecteddate').type(dayjs().add(20, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
    });
    And('I select task for OT Initial Eval', () => {
        cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
        cy.wait(5000)
        cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(22)').click()//select task
    });
    And('I select clinician for OT Initial Eval', () => {
        cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
        cy.wait(5000)
        cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
    });
    And('I uncheck Create an MD Order for OT Initial Eval', () => {
        cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
    });
    And('I save task created for OT Initial Eval', () => {
        cy.get('.mhc__btn-affirmative').click()//save task created
        cy.wait(10000)
    });

    // Schedule ST - Initial Eval
    And('I schedule or click date for ST Initial Eval', () => {
        cy.get('#save > li > button').click()//clicking the calendar overly icon
    });
    And('I input task date for ST Initial Eval', () => {
        cy.get('#selecteddate').type(dayjs().add(22, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
    });
    And('I select task for ST Initial Eval', () => {
        cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
        cy.wait(5000)
        cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(27)').click()//select task
    });
    And('I select clinician for ST Initial Eval', () => {
        cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
        cy.wait(5000)
        cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
    });
    And('I uncheck Create an MD Order for ST Initial Eval', () => {
        cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
    });
    And('I save task created for ST Initial Eval', () => {
        cy.get('.mhc__btn-affirmative').click()//save task created
        cy.wait(10000)
    });

    // Schedule MSW - Assessment
    And('I schedule or click date for MSW Assessment', () => {
        cy.get('#save > li > button').click()//clicking the calendar overly icon
    });
    And('I input task date for MSW Assessment', () => {
        cy.get('#selecteddate').type(dayjs().add(24, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
    });
    And('I select task for MSW Assessment', () => {
        cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
        cy.wait(5000)
        cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(31)').click()//select task
    });
    And('I select clinician for MSW Assessment', () => {
        cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
        cy.wait(5000)
        cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
    });
    And('I uncheck Create an MD Order for MSW Assessment', () => {
        cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
    });
    And('I save task created for MSW Assessment', () => {
        cy.get('.mhc__btn-affirmative').click()//save task created
        cy.wait(10000)
    });

    // Schedule MSW - Follow-up Visit
    And('I schedule or click date for MSW Follow up Visit', () => {
        cy.get('#save > li > button').click()//clicking the calendar overly icon
    });
    And('I input task date for MSW Follow up Visit', () => {
        cy.get('#selecteddate').type(dayjs().add(26, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
    });
    And('I select task for MSW Follow up Visit', () => {
        cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
        cy.wait(5000)
        cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(32)').click()//select task
    });
    And('I select clinician for MSW Follow up Visit', () => {
        cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
        cy.wait(5000)
        cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
    });
    And('I uncheck Create an MD Order for MSW Follow up Visit', () => {
        cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
    });
    And('I save task created for MSW Follow up Visit', () => {
        cy.get('.mhc__btn-affirmative').click()//save task created
        cy.wait(10000)
    });

    // Schedule CHHA - HHA Visit
    And('I schedule or click date for CHHA HHA Visit', () => {
        cy.get('#save > li > button').click()//clicking the calendar overly icon
    });
    And('I input task date for CHHA HHA Visit', () => {
        cy.get('#selecteddate').type(dayjs().add(31, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
    });
    And('I select task for CHHA HHA Visit', () => {
        cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
        cy.wait(5000)
        cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(34)').click()//select task
    });
    And('I select clinician for CHHA HHA Visit', () => {
        cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
        cy.wait(5000)
        cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
    });
    And('I uncheck Create an MD Order for CHHA HHA Visit', () => {
        cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
    });
    And('I save task created for CHHA HHA Visit', () => {
        cy.get('.mhc__btn-affirmative').click()//save task created
        cy.wait(10000)
    });
    
    


    
    

