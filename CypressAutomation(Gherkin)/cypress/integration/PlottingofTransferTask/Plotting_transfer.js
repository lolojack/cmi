Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
  });

  const dayjs = require('dayjs')
  
  
  Given('I Login', () => {
    cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jow P. Jr')  //search the added patient 
        cy.wait(5000) 
    });

    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(5000)
    });

    // Schedule RN-OASIS-D1 Transfer (not discharged)
    And('I schedule or click date for transfer not discharged', () => {
        cy.get('#save > li > button').click()//clicking the calendar overly icon
    });
    And('I input task date for transfer not discharged', () => {
        cy.get('#selecteddate').type(dayjs().add(45, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
    });
    And('I select task for transfer not discharged', () => {
        cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
        cy.wait(5000)
        cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(6)').click()//select task
    });
    And('I select clinician for transfer not discharged', () => {
        cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
        cy.wait(5000)
        cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
    });
    And('I input visit date', () => {
        cy.get('#visitdate').type(dayjs().add(45, 'day').format('MM/DD/YYYY')) //input visit date
    });
    And('I save task created for transfer not discharged', () => {
        cy.get('.mhc__btn-affirmative').click()//save task created
        cy.wait(10000)
    });
