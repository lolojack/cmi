Given('I open Realtime Workflow login page', () => {
    cy.visit('https://qado.medisource.com/login'); //visit page   
  });
  
  When('I type in username "vincent@geeksnest" and password "Tester2021@"', () => { 
        cy.get('#loginemail').type("vincent@geeksnest") //input username
        cy.get('#loginpassword').type("Tester2021@") //input password
  })

//   When('I type in', (datatable) => { 
//     datatable.hashes().forEach(element => {
//         cy.get('#loginemail').type(element.username)
//         cy.get('#loginpassword').type(element.password)
//     })
//   })
  
  When('I click on Login button', () => {
    cy.get('.btn').contains('Login').should('be.visible').click() //click login button
  });
  
  Then('It Redirect to Dashboard', () => {
    cy.wait(10000)
    cy.url().should('be.equal', 'https://qado.medisource.com/dashboard')
  });

