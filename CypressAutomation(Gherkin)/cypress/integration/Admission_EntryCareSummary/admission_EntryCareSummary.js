Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
  });

  const dayjs = require('dayjs')
  
  
Given('I Login', () => {
    cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jendo P. Jr')  //search the added patient 
        cy.wait(10000) 
    });
    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(5000)
    });
    And('I click Communication Note', () => {
        cy.get(':nth-child(4) > .navtitle').click();//Communication Note
    });
    And('I click new', () => {
        cy.get('.col-sm-12 > .pull-right > .btn').click() //clicking new
    });
    And('I click 30-Day Care Summary', () => {
        cy.get('[ui-sref="patientcare.careSummary30daysCreate"]').click() //clicking 30-day care summary
    });
    And('I input conference date', () => {
        cy.get('#date').type(dayjs().add(58, 'day').format('MM/DD/YYYY')) ;
    });
    And('I input other diagnoses', () => {
        cy.get('.tags > .ng-pristine').type('Test') //inputting other 
    });
    //SCIC/Change in Diagnosis/Events/Physician Orders
    And('I select all SCIC-Change in Diagnosis-Events-Physician Orders', () => {
        cy.get('[rname="events"]').click({multiple:true})
    });
    And('I input other and comment', () => {
        cy.get(':nth-child(3) > tbody > :nth-child(2) > td > .row > .static > .fg-line > .global__txtbox').type('Test') //inputting other
        cy.get('[style="width: 92%"] > .global__txtbox').type('Test') //inputting comment
    });
    //VS Monitor
    And('I input comment for VS Monitor', () => {
        cy.get(':nth-child(4) > tbody > :nth-child(5) > td > [style="width: 90%"] > .form-control').type('Test') //inputting comment
    });
    //Medication Management
    And('I click yes for Patient compliant and Patient adherent', () => {
        cy.get(':nth-child(5) > :nth-child(2) > :nth-child(1) > .ng-valid').click() //clicking yes Patient compliant?
        cy.get(':nth-child(2) > :nth-child(3) > .ng-pristine').click() //clicking yes Patient adherent? 
    });
    //Laboratory/Diagnostic Tests
    And('I input comment for Laboratory or Diagnostic Tests', () => {
        cy.get(':nth-child(6) > :nth-child(2) > .form-control').type('Test') //inputting comment
    });
    //Procedures
    And('I input comment for Procedures', () => {
        cy.get(':nth-child(2) > td > [style="width: 90%"] > .form-control').type('Test') //inputting comment
    });
     //Assessment Summary
    And('I input comment for sensory status, integumentary, endocrine, respiratory, cardiovascular, Upper GI, Lower GI, Genitourinary, Neurologic and Musculoskeletal', () => {
        cy.get('[width="67%"] > [style="width: 88%"] > .global__txtbox').type('Test') //inputting comment Sensory Status
        cy.get(':nth-child(8) > tbody > :nth-child(3) > :nth-child(4)').type('Test') //inputting comment Integumentary
        cy.get(':nth-child(4) > :nth-child(4) > [style="width: 88%"] > .global__txtbox').type('Test') //inputting comment Endocrine
        cy.get(':nth-child(5) > :nth-child(4) > [style="width: 88%"] > .global__txtbox').type('Test') //inputting comment Respiratory
        cy.get(':nth-child(6) > :nth-child(4) > [style="width: 88%"] > .global__txtbox').type('Test') //inputting comment Cardiovascular
        cy.get(':nth-child(7) > :nth-child(4) > [style="width: 88%"] > .global__txtbox').type('Test') //inputting comment Upper GI
        cy.get(':nth-child(8) > :nth-child(4) > [style="width: 88%"] > .global__txtbox').type('Test') //inputting comment Lower GI
        cy.get(':nth-child(9) > :nth-child(4) > [style="width: 88%"] > .global__txtbox').type('Test') //inputting comment Genitourinary
        cy.get(':nth-child(10) > :nth-child(4) > [style="width: 88%"] > .global__txtbox').type('Test') //inputting comment Neurologic
        cy.get(':nth-child(11) > :nth-child(4) > [style="width: 88%"] > .global__txtbox').type('Test') //inputting comment Musculoskeletal
    });
    And('I input comment for Assessment Summary', () => {
        cy.get('[colspan="3"] > .form-control').type('Test') //inputting comment
    });
    And('I input reasons for home health', () => {
        cy.get(':nth-child(9) > tbody > :nth-child(2) > td > .form-control').type('Test') //inputting Reasons for Home health
    });
    And('I input homebound status', () => {
        cy.get(':nth-child(10) > tbody > :nth-child(2) > td > .form-control').type('Test') //inputting Homebound Status
    });
    And('I input caregiving status', () => {
        cy.get(':nth-child(11) > tbody > :nth-child(2) > td > .form-control').type('Test') //inputting Caregiving Status
    });
    //Physician Orders for Disciplines and Ancillary Referrals
    And('I input comment for Physician Orders for Disciplines and Ancillary Referrals', () => {
        cy.get(':nth-child(12) > tbody > :nth-child(5) > td > [style="width: 90%"] > .form-control').type('Test') //inputting comment
    });
    And('I input care summary', () => {
        cy.get(':nth-child(13) > tbody > :nth-child(2) > td > .form-control').type('Test') //inputting Care Summary
    });
    And('I input progress towards goals', () => {
        cy.get(':nth-child(14) > tbody > :nth-child(2) > td > .form-control').type('Test') //inputting Progress towards Goals
    });
    And('I input recommendations', () => {
        cy.get(':nth-child(15) > tbody > :nth-child(2) > td > .form-control').type('Test') //inputting Recommendations
    });
    And('I select team conference reviewers', () => {
        cy.get('#conferencereviewers_chosen > ul').click() //clicking dropdown Team Conference Reviewers
        cy.get('#conferencereviewers_chosen > div > ul > li:nth-child(3)').click() //clicking result
    });
    And('I click save button', () => {
        cy.get('.btn__success').click() // clicking save button
        cy.wait(5000)
    });

Given('I Login1', () => {
    cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jendo P. Jr')  //search the added patient 
        cy.wait(10000) 
    });
    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(5000)
    });
    And('I click Communication Note', () => {
        cy.get(':nth-child(4) > .navtitle').click();//Communication Note
    });
    And('I click new', () => {
        cy.get('.col-sm-12 > .pull-right > .btn').click() //clicking new
    });
    And('I click Transfer Summary', () => {
        cy.get('[ui-sref="patientcare.careSummaryTransferCreate"]').click() //clicking Transfer Summary
    });
    And('I input transfer date', () => {
        cy.get('#transfer_date').type('05302022', {force:true}) //inputting transfer date
    });
    //Physician
    And('I select physician', () => {
        cy.get(':nth-child(2) > :nth-child(1) > .select > .chosen-container > .chosen-single').click() //clicking dropdown physician
        cy.get('#CommTransferSummary > div > div.table-responsive.max__width_wrapper > fieldset > table > tbody > tr > td > table.global__table.b-0 > tbody > tr:nth-child(2) > td:nth-child(1) > div > div > div > ul > li:nth-child(1)').click() //clicking result
    });
    //Transfer to (click all)
    And('I select all for transfer', () => {
        cy.get('[rname="transfer_to"]').click({multiple:true})
        cy.get(':nth-child(3) > .global__txtbox').type('Test') //inputting other
    });
    //Reason for Transfer (click all)
    And('I select all for reasons for transfer', () => {
        cy.get('[rname="transfer_reasons"]').click({multiple:true})
        cy.get(':nth-child(3) > td > .global__txtbox').type('Test') //inputting other
    });
    //Current Physical, Mental and Emotional Status (click all)
    And('I select all for Current Physical, Mental and Emotional Status', () => {
        cy.get('[rname="statuses"]').click({multiple:true})
        cy.get('.col-sm-9 > .fg-line > .global__txtbox').type('Test') //inputting other
    });
    //Current Functional Status
    And('I click independent for ADL', () => {
        cy.get(':nth-child(2) > .oasis__answer > .w-100 > tbody > :nth-child(1) > td > .row > :nth-child(1) > .radio > .ng-valid').click() //clicking independent
    });
    And('I click independent for mobility', () => {
        cy.get(':nth-child(1) > td > .row > :nth-child(1) > .radio > .ng-pristine').click() //clicking independent
    });
    And('I select all for assistive device', () => {
        cy.get('[rname="assistive_devices"]').click({multiple:true})
        cy.get(':nth-child(2) > tbody > tr > .oasis__answer > div.d-ib > .global__txtbox').type('Test') //inputting other
    });
    And('I input summary of care provided', () => {
        cy.get('.col-xs-12 > .fg-line > .form-control').type('Test') //inputting Summary of Care Provided
    });
    And('I select all for disciplines or visits provided', () => {
        cy.get('[rname="disciplines"]').click({multiple:true})
        cy.get(':nth-child(10) > tbody > tr > .oasis__answer > div.d-ib > .global__txtbox').type('Test') //inputting other
    });
    And('I input date for physician', () => {
        cy.get('#notification_date_physician').type('05302022') //inputting date physician
    });
    And('I input date for Patient or Family or Caregiver', () => {
        cy.get('#notification_date_patient').type('05302022') //inputting date Patient/Family/Caregiver
    });
    And('I click fax for send to MD via', () => {
        cy.get('.table.m-b-15 > tbody > :nth-child(2) > :nth-child(4) > div > :nth-child(1) > .ng-pristine').click() //clicking fax
    });
    And('I click save button', () => {
        cy.get('.btn__success').click() //clicking Save Button
    });

Given('I Login2', () => {
    cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jendo P. Jr')  //search the added patient 
        cy.wait(10000) 
    });
    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(5000)
    });
    And('I click Communication Note', () => {
        cy.get(':nth-child(4) > .navtitle').click();//Communication Note
    });
    And('I click new', () => {
        cy.get('.col-sm-12 > .pull-right > .btn').click() //clicking new
    });
    And('I click Discharge Summary', () => {
        cy.get('[ui-sref="patientcare.careSummaryDischargeCreate()"]').click() //clicking Discharge Summary
        cy.wait(10000)
    });
    And('I input discharge date', () => {
        cy.get('#dischargeDate').type(dayjs().add(58, 'day').format('MM/DD/YYYY')) ;
    });
    And('I input secondary diagnoses', () => {
        cy.get(':nth-child(2) > .form-control').type('Test') //inputting secondary diagnoses
    });
    //Reason(s) for Home Health Admission (click all)
    And('I select all for home health admission', () => {
        cy.get('[rname="reasAdm"]').click({multiple:true})
        cy.get(':nth-child(2) > .fg-line > .form-control').type('Test') //inputting other reason
    });
    //Reason(s) for Discharge (click all)
    And('I select all for reasons for discharge', () => {
        cy.get('[rname="reasonDisList"]').click({multiple:true})
        cy.get('.col-xs-6 > .d-ib > .global__txtbox').type('Test') //inputting other reason
    });
    //Current Physical, Mental and Emotional Status (click all)
    And('I select all for Current Physical, Mental and Emotional Status', () => {
        cy.get('[rname="physMenEmList"]').click({multiple:true})
        cy.get('.col-sm-9 > .fg-line > .global__txtbox').type('Test') //inputting other reason
    });
    //Current Functional Status
    And('I click independent for ADL', () => {
        cy.get(':nth-child(2) > .oasis__answer > .w-100 > tbody > :nth-child(1) > td > .row > :nth-child(1) > .radio > .ng-valid').click() //clicking independent
    });
    And('I click independent for mobility', () => {
        cy.get(':nth-child(3) > .oasis__answer > .w-100 > tbody > :nth-child(1) > td > .row > :nth-child(1) > .radio > .ng-valid').click() //clicking independent
    });
    //Assistive Device: (click all)
    And('I select all for assistive device', () => {
        cy.get('[rname="assisDevList"]').click({multiple:true})
        cy.get(':nth-child(5) > tbody > tr > .oasis__answer > div.d-ib > .global__txtbox').type('Test') //inputting other
    });
    And('I click 1 for discharge disposition', () => {
        cy.get('.m-t-0 > .ng-valid').click() //clicking 1 for Discharge Disposition
    });
    And('I input summary of care provided', () => {
        cy.get('.oasis__answer > .col-xs-12 > .fg-line > .form-control').type('Test') //inputting Summary of Care Provided
    });
    //Disciplines/visits provided
    And('I select all for disciplines or visits provided', () => {
        cy.get('[rname="invDiscList"]').click({multiple:true})
        cy.get(':nth-child(8) > tbody > tr > .oasis__answer > div.d-ib > .global__txtbox').type('Test') //inputting other
    });
    And('I click goals met for Treatment Goals Achieved', () => {
        cy.get(':nth-child(9) > tbody > tr > .oasis__answer > div > :nth-child(1) > .ng-pristine').click() //clicking goals met for Treatment Goals Achieved
    });
    //Discharge Notification Date
    And('I input date for physician', () => {
        cy.get('#physicianDischargeDate').type('05302022') //inputting date physician
    });
    And('I input date for Patient or Family or Caregiver', () => {
        cy.get('#patFamCare').type('05302022') //inputting date Patient/Family/Caregiver
    });
    //Send to MD via	
    And('I click fax for send to MD via', () => {
        cy.get(':nth-child(10) > tbody > :nth-child(2) > :nth-child(4) > div > :nth-child(1) > .ng-pristine').click() //clicking fax
    });
    And('I click save button', () => {
        cy.get('.btn__success').click() //clicking Save Button
    });

Given('I Login3', () => {
    cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jendo P. Jr')  //search the added patient 
        cy.wait(10000) 
    });
    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(5000)
    });
    And('I click Communication Note', () => {
        cy.get(':nth-child(4) > .navtitle').click();//Communication Note
    });
    And('I click new', () => {
        cy.get('.col-sm-12 > .pull-right > .btn').click() //clicking new
    });
    And('I click Discharge Instruction', () => {
        cy.get('[ui-sref="patientcare.careSummaryDischargeInsCreate"]').click() //clicking Discharge Instruction
        cy.wait(10000)
    });
    And('I input discharge date', () => {
        cy.get('#dischargeDate').type(dayjs().add(58, 'day').format('MM/DD/YYYY')) ;
    });
    And('I select physician', () => {
        cy.get(':nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking dropdown physician
        cy.get('#parent > div > form > div > div.table-responsive.max__width_wrapper > fieldset > table > tbody > tr > td > table:nth-child(1) > tbody > tr:nth-child(2) > td:nth-child(2) > div > div > div > ul > li:nth-child(1)').click() //clicking result
    });
    //We are pleased to have provided service to you. The following discharge instructions were reviewed with you and/or your caregiver/s during the final visit by agency staff. You are to:
    //Keep your scheduled appointment with Dr.
    And('I select doctor', () => {
        cy.get(':nth-child(2) > .oasis__answer > [style="min-width:250px; margin: 6px;"] > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown doctor
        cy.get('#parent > div > form > div > div.table-responsive.max__width_wrapper > fieldset > table > tbody > tr > td > table.table.global__table.m-t-15.ng-scope > tbody > tr:nth-child(2) > td > span:nth-child(2) > div > div > div > div > ul > li:nth-child(1)').click() //clicking result doctor
    });
    And('I input date appointment', () => {
        cy.get('#appointmentDate').type('05302022') //inputting date appointment
    });
    And('I select doctor1', () => {
        cy.get(':nth-child(3) > .oasis__answer > [style="min-width:250px; margin: 6px;"] > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown doctor
        cy.get('#parent > div > form > div > div.table-responsive.max__width_wrapper > fieldset > table > tbody > tr > td > table.table.global__table.m-t-15.ng-scope > tbody > tr:nth-child(3) > td > span:nth-child(2) > div > div > div > div > ul > li:nth-child(1)').click() //clicking result doctor
    });
    And('I input date appointment1', () => {
        cy.get('#appointmentDateTwo').type('05302022') //inputting date appointment
    });
    And('I click Continue to take the medications as prescribed by your physician', () => {
        cy.get('.checkbox > .m-r-10 > .ng-valid').click() //clicking Continue to take the medications as prescribed by your physician.
    });
    And('I click Medication list attached', () => {
        cy.get('[chk="data.medList"] > .ng-valid').click() //clicking Medication list attached
    });
    And('I input Additional medication instructions', () => {
        cy.get(':nth-child(7) > .oasis__answer > .tblChk > .fg-line > .form-control').type('Test') //inputting Additional medication instructions:
    });
    //Continue with the home program as instructed by therapist/nurse. Refer to the handouts provided.
    And('I input additional instruction', () => {
        cy.get(':nth-child(11) > .oasis__answer > .tblChk > .fg-line > .form-control').type('Test') //inputting additional instruction
    });
    //Continue with the skin/wound care as ordered by your physician and instructed by your nurse.
    And('I input wound care instruction', () => {
        cy.get(':nth-child(13) > .oasis__answer > .tblChk > .fg-line > .form-control').type('Test') //inputting wound care instruction
    });
    And('I input Follow through with community resource or organization to which you been referred', () => {
        cy.get('.oasis__answer > .cont-opt > .fg-line > .global__txtbox').type('Test')
    });
    And('I input other instruction', () => {
        cy.get('.oasis__answer > .fg-line > .form-control').type('Test')
    });
    And('I click save button', () => {
        cy.get('.btn__success').click() //clicking Save Button
    });

Given('I Login4', () => {
    cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jendo P. Jr')  //search the added patient 
        cy.wait(10000) 
    });
    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(5000)
    });
    And('I click Communication Note', () => {
        cy.get(':nth-child(4) > .navtitle').click();//Communication Note
    });
    And('I click new', () => {
        cy.get('.col-sm-12 > .pull-right > .btn').click() //clicking new
    });
    And('I click Case Conference', () => {
        cy.get('[ui-sref="patientcare.careSummaryCaseConferenceCreate"]').click() //clicking Case Conference
        cy.wait(10000)
    });
    And('I input date created', () => {
        cy.get('#order_date').type('05302022') //inputting date created
    });
    And('I input home health reason', () => {
        cy.get('.b-r-n > fieldset > .form-control').type('Test') //inputting home health reason
    });
    And('I input purpose of the conference', () => {
        cy.get('.oasis__answer > fieldset > .form-control').type('Test') //inputting Purpose of the Conference
    });
    And('I click and input for Assessment of patient status and current needs by', () => {
        cy.get('[style="float:left;"] > .checkbox > .ng-valid').click() //clicking Assessment of patient status and current needs by:
        cy.get('[style="margin-left:330px;"] > .global__txtbox').type('Test') //inputting Assessment of patient status and current needs by:
        cy.get('.ng-scope > .oasis__answer > fieldset > .form-control').type('Test')
    });
    And('I click Plan of actions to be taken, by whom and timeframe', () => {
        cy.get('.global__table-section > .checkbox > .ng-valid').click() //clicking Plan of actions to be taken, by whom and timeframe
    });
    //Plan of actions to be taken, by whom and timeframe
    And('I input all Plan of actions to be taken', () => {
        cy.get(':nth-child(2) > :nth-child(1) > .fg-line > .global__txtbox').type('Test')
        cy.get(':nth-child(3) > :nth-child(1) > .fg-line > .global__txtbox').type('Test')
        cy.get(':nth-child(4) > :nth-child(1) > .fg-line > .global__txtbox').type('Test')
    });
    And('I input all By whom', () => {
        cy.get(':nth-child(2) > :nth-child(2) > .fg-line > .global__txtbox').type('Test')
        cy.get(':nth-child(3) > :nth-child(2) > .fg-line > .global__txtbox').type('Test')
        cy.get(':nth-child(4) > :nth-child(2) > .fg-line > .global__txtbox').type('Test')
    });
    And('I input all Time frame, if any', () => {
        cy.get(':nth-child(2) > :nth-child(3) > .fg-line > .global__txtbox').type('Test')
        cy.get(':nth-child(3) > :nth-child(3) > .fg-line > .global__txtbox').type('Test')
        cy.get(':nth-child(4) > :nth-child(3) > .fg-line > .global__txtbox').type('Test')
    });
    And('I select all Case Conference Participants', () => {
        cy.get('.p-0 > .ng-isolate-scope > .ng-valid').click()
        cy.get(':nth-child(2) > .ng-isolate-scope > .ng-valid').click()
        cy.get(':nth-child(3) > .ng-isolate-scope > .ng-pristine').click()
    });
    And('I input comment', () => {
        cy.get('.p-t-0 > .ng-pristine').type('Test') //inputting comment
    });
    And('I click save button', () => {
        cy.get('.btn__success').click() //clicking Save Button
    });





