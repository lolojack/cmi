// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
Cypress.Commands.add("parseXlsx", (inputFile) => {
  return cy.task('parseXlsx', { filePath: inputFile })
  });
  
Cypress.Commands.add('login', (name, password) => {       // command for login with session
    cy.session([name, password], () => {                    // session() restores a cached session
      cy.visit('https://qado.medisource.com/login');
          cy.viewport(1920, 924);//setting your windows size
          cy.get('#loginemail').type(name);
          cy.get('#loginpassword').type(password);
          cy.get('.btn').click();
          cy.url().should('not.contain', '/login');
    })
  })
